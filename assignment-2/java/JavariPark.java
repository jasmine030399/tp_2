import cages.*;
import animals.*;
import java.util.Scanner;
import java.util.ArrayList;

public class JavariPark {
    private static ArrayList<Animals> catList = new ArrayList<>();
    private static ArrayList<Animals> lionList = new ArrayList<>();
    private static ArrayList<Animals> eagleList = new ArrayList<>();
    private static ArrayList<Animals> parrotList = new ArrayList<>();
    private static ArrayList<Animals> hamsterList = new ArrayList<>();

    public static void main(String[] args) {
        String[] types = { "cat", "lion", "eagle", "parrot", "hamster" };
        int[] counts = new int[5];
        Scanner data = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        // Scanner looping
        for (int i = 0; i < 5; i++) {
            System.out.print(types[i] + ": ");
            int numAnimal = data.nextInt();
            // int numAnimal = Integer.parseInt(data.nextLine());
            counts[i] = numAnimal;
            // data.nextLine();
            if (numAnimal > 0) {
                JavariPark.selectAnimal(types[i]);
            } else {
                continue;
            }
        }
        System.out.println("Animals have been successfully recorded!\n");
        JavariPark.printArrange();
        JavariPark.visitAnimal();
    }

    private static void selectAnimal(String type) {
        Scanner animaldata = new Scanner(System.in);
        System.out.println("Provide the information of " + type + "(s):");
        String animalInfo = animaldata.nextLine();
        String[] betweenAnimals = animalInfo.split(","); // separate animal's data

        for (int i = 0; i < betweenAnimals.length; i++) {
            String[] nameLengthAnimal = betweenAnimals[i].split("\\|");
            String animalName = nameLengthAnimal[0];
            int animalLength = Integer.parseInt(nameLengthAnimal[1]);

            // Initiate by using polymorphism
            if (type.equalsIgnoreCase("cat")) { // compare 2 strings, uppercase lowercase
                Animals theCat = new Cat(animalName, animalLength);
                catList.add(theCat);
            } else if (type.equalsIgnoreCase("hamster")) {
                Animals theHamster = new Hamsters(animalName, animalLength);
                hamsterList.add(theHamster);
            } else if (type.equalsIgnoreCase("parrot")) {
                Animals theParrot = new Parrots(animalName, animalLength);
                parrotList.add(theParrot);
            } else if (type.equalsIgnoreCase("lion")) {
                Animals theLion = new Lion(animalName, animalLength);
                lionList.add(theLion);
            } else if (type.equalsIgnoreCase("eagle")) {
                Animals theEagle = new Eagle(animalName, animalLength);
                eagleList.add(theEagle);
            }
        }
    }

    private static void printArrange() {
        System.out.println("=============================================\n" + "Cage arrangement:");
        System.out.println();
        if (catList.size() > 0) {
            //
            Cages.arrange(catList);
            Cages.afterArrange();
        }
        if (lionList.size() > 0) {
            Cages.arrange(lionList);
            Cages.afterArrange();
        }
        if (eagleList.size() > 0) {
            Cages.arrange(eagleList);
            Cages.afterArrange();
        }
        if (parrotList.size() != 0) {
            Cages.arrange(parrotList);
            Cages.afterArrange();
        }
        if (hamsterList.size() != 0) {
            Cages.arrange(hamsterList);
            Cages.afterArrange();
        }

        System.out.format("ANIMALS NUMBER:\ncat:%d\nlion:%d\nparrot:%d\neagle:%d\nhamster:%d\n\n", catList.size(),
                lionList.size(), parrotList.size(), eagleList.size(), hamsterList.size());
        System.out.println("============================================= ");

    }

    private static void visitAnimal() {
        while (true) {
            System.out.print("Which animal you want to visit?\n"
                    + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)\n");
            Scanner visitInput = new Scanner(System.in);
            String checkNum = visitInput.nextLine();

            // Boolean to check the animal's availability
            boolean available = false;

            if (checkNum.equalsIgnoreCase("1")) {
                System.out.print("Mention the name of cat you want to visit:");
                String catName = visitInput.next();
                for (Animals theCat : catList) {
                    if (catName.equalsIgnoreCase(theCat.getName())) {
                        System.out.println("You are visiting " + catName + " (cat) now, what would you like to do?\n"
                                + "1: Brush the fur 2: Cuddle");
                        int catMethod = visitInput.nextInt();
                        ((Cat) theCat).choose(catMethod); // Cast the type from Animal to Cat
                        available = true;
                    }
                }
                if (available == false) {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }

            } else if (checkNum.equalsIgnoreCase("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String eagleName = visitInput.nextLine();
                for (Animals theEagle : eagleList) {
                    if (eagleName.equalsIgnoreCase(theEagle.getName())) {
                        System.out.println("You are visiting " + eagleName
                                + " (eagle) now, what would you like to do?\n" + "1: Order to fly");
                        int eagleMethod = visitInput.nextInt();
                        ((Eagle) theEagle).choose(eagleMethod); // Cast the type from Animal to Eagle
                        available = true;
                    }
                }
                if (available == false) {
                    System.out.println("There is no eagle with that name! Back to the office!\n");
                }

            } else if (checkNum.equalsIgnoreCase("3")) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String hamsterName = visitInput.nextLine();
                for (Animals theHamster : hamsterList) {
                    if (hamsterName.equalsIgnoreCase(theHamster.getName())) {
                        System.out.println( "You are visiting " + hamsterName + " (hamster) now, what would you like to do?\n"
                                + "1: See it gnawing 2: Order to run in the hamster wheel");
                        int hamsterMethod = visitInput.nextInt();
                        ((Hamsters) theHamster).choose(hamsterMethod); // Cast the type from Animal to Hamster
                        available = true;
                    }
                }
                if (available == false) {
                    System.out.println("There is no hamster with that name! Back to the office!\n");
                }
            } else if (checkNum.equalsIgnoreCase("4")) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String parrotName = visitInput.nextLine();
                for (Animals theParrot : parrotList) {
                    if (parrotName.equalsIgnoreCase(theParrot.getName())) {
                        System.out.println("You are visiting " + parrotName + " (parrot) now, what would you like to do?\n"
                                + "1: Order to fly 2: Do conversation");
                        int parrotMethod = visitInput.nextInt();
                        ((Parrots) theParrot).choose(parrotMethod); // Cast the type from Animal to Parrot
                        available = true;
                    }
                }
                if (available == false) {
                    System.out.println("There is no parrot with that name! Back to the office!\n");
                }
            } else if (checkNum.equalsIgnoreCase("5")) {
                System.out.print("Mention the name of lion you want to visit: ");
                String lionName = visitInput.nextLine();
                for (Animals theLion : lionList) {
                    if (lionName.equalsIgnoreCase(theLion.getName())) {
                        System.out.println("You are visiting " + lionName + " (lion) now, what would you like to do?\n"
                                + "1: See it hunting 2: Brush the mane 3: Disturb it");
                        int lionMethod = visitInput.nextInt();
                        ((Lion) theLion).choose(lionMethod); // Cast the type from Animal to Lion
                        available = true;
                    }
                }
                if (available == false) {
                    System.out.println("There is no lion with that name! Back to the office!\n");
                }
            } else {
                break;
            }
        }
    }
}

