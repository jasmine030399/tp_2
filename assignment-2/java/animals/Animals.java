package animals;
public class Animals{
	private String name;
	private int length;
	private boolean wild;
	public Animals(String name, int length, boolean wild){
		this.name = name;
		this.length = length;
		this.wild = wild;
	}
	
	public void setName(String name){ this.name = name;}
	
	public void setLength(int length){ this.length = length;}
	
	public void setWild(boolean wild){ this.wild = wild;}
	
	public String getName(){ return name;}
	
	public int getLength(){ return length;}
	
	public boolean getWild(){ return wild;}
	
	public void makeVoice(String suara){
		System.out.format("%s make a voice : %s",name, suara);
	}
	
	public void doNothing(){
		System.out.print("You do nothing. . .");
	}
	
}
	
	

