package animals;
public class Lion extends Animals {
	public Lion(String name, int length){
		super(name, length,true);
	}
	
	public void hunt(){
		makeVoice("Err....");
	}
	
	public void goodmood(){
		makeVoice("Hauhhmm!");
	}
	
	public void feeldisturbed(){
		makeVoice("HAAHUM!!");
	}
}