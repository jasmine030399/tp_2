package animals;
public class Hamsters extends Animals{
	public Hamsters(String name, int length){
		super(name, length, false);
	}
	
	public void SeeItGwaning(){
		makeVoice("Ngkkrit.. Ngkkrrriiit");
	}
	
	public void OrderRun(){
		makeVoice("Trrr... Trrr...");
	}
}