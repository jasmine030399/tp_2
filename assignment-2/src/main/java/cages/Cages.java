package cages;

import animals.*;
import java.util.ArrayList;

public class Cages {

    protected static ArrayList<Animals> listAnimals;
    protected static ArrayList<String> Level1;
    protected static ArrayList<String> Level2;
    protected static ArrayList<String> Level3;
    protected static int border1;
    protected static int border2;
    protected static String location = "";
    protected static String levelone;
    protected static String leveltwo;
    protected static String levelthree;
    protected static String leveloneafter;
    protected static String leveltwoafter;
    protected static String levelthreeafter;
    protected char cageSize;
    protected String cageType;

    public Cages(String name, int length, String animalsType) {
        if (animalsType.equals("cat") || animalsType.equals("parrots") || animalsType.equals("hamsters")) {
            if (length < 45) {
                this.cageSize = 'A';
            } else if (length >= 45 && length < 60) {
                this.cageSize = 'B';
            } else {
                this.cageSize = 'C';
            }
            this.cageType = "indoor";
        }
        else if (animalsType.equals("lion") || animalsType.equals("eagle")) {
            if (length < 75) {
                this.cageSize = 'A';
            } else if (length >= 75 && length <= 90) {
                this.cageSize = 'B';
            } else {
                this.cageSize = 'C';
            }
            this.cageType = "outdoor";
        }
    }

    public char getCageSize() {
        return cageSize;
    }

    public String getCageType() {
        return cageType;
    }

    public static void borderLevel(ArrayList<Animals> listAnimals) {
        Level1 = new ArrayList<String>();
        Level2 = new ArrayList<String>();
        Level3 = new ArrayList<String>();
        int lengthAnimals = listAnimals.size();

        border1 = lengthAnimals/3;
        border2 = 0;
        if (lengthAnimals % 3 == 2) {
            border2 = (border1 * 2) + 1;
        } else {
            border2 = border1 * 2;
        }

        if (border1 == 0) {
            border1 = 1;
            if (lengthAnimals == 1) {
                border2 = 1;
            } else {
                border2 = 2;
            }
        }
    }

    public static void arrange(ArrayList<Animals> listAnimals) {
        Cages.borderLevel(listAnimals);
        levelone = "level 1: ";
        leveltwo = "level 2: ";
        levelthree = "level 3: ";

        for (int i = 0; i < border1; i++) {
            Animals eachAnimals = listAnimals.get(i);
            Cages animalsCage = new Cages (eachAnimals.getName(), eachAnimals.getLength(), eachAnimals.getAnimalsType());
            String animalsPrint = String.format("%s(%d-%s)", eachAnimals.getName(), eachAnimals.getLength(), animalsCage.getCageSize());
            Level1.add(animalsPrint);
            levelone += animalsPrint + ",";
            location = animalsCage.getCageType();
        }

        for (int i = border1; i < border2; i++) {
            Animals eachAnimals = listAnimals.get(i);
            Cages animalsCage = new Cages(eachAnimals.getName(), eachAnimals.getLength(), eachAnimals.getAnimalsType());
            String animalsPrint = String.format("%s(%d-%s)", eachAnimals.getName(), eachAnimals.getLength(), animalsCage.getCageSize());
            Level2.add(animalsPrint);
            leveltwo += animalsPrint + ",";
        }

        for (int i = border2; i < listAnimals.size(); i++) {
            Animals eachAnimals = listAnimals.get(i);
            Cages animalsCage = new Cages(eachAnimals.getName(), eachAnimals.getLength(), eachAnimals.getAnimalsType());
            String animalsPrint = String.format("%s(%d-%s)", eachAnimals.getName(), eachAnimals.getLength(), animalsCage.getCageSize());
            Level3.add(animalsPrint);
            levelthree += animalsPrint + ",";
        }

        System.out.println("location: " + location);
        System.out.println(levelthree + "\n" + leveltwo + "\n" + levelone + "\n");
    }

    public static void afterArrange() {
        leveloneafter = "level 1 : ";
        leveltwoafter = "level 2 : ";
        levelthreeafter = "level 3 : ";

        // Rearrange from level 3 to level 1
        for (int i = Level3.size(); i > 0; i--) {
            leveloneafter += Level3.get(i - 1) + ",";
        }

        // Rearrange from level 1 to level 2
        for (int i = Level1.size(); i > 0; i--) {
            leveltwoafter += Level1.get(i - 1) + ",";
        }

        // Rearrange from level 2 to level 3
        for (int i = Level2.size(); i > 0; i--) {
            levelthreeafter += Level2.get(i - 1) + ",";
        }
        System.out.println("After rearrangement...\n" + levelthreeafter + "\n" + leveltwoafter + "\n" + leveloneafter + "\n");
    }
}
