
import java.util.Random;
package animals;

public class Cat extends Animals{
	public Cat(String name, int length){
		super(name, length, false);
	}
	
	public void BrushTheFur(){
		makeVoice("Nyaaan...");
	}
	
	public void Cuddle(){
		String[] sound = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
		Random random = new Random(4);
		int select = random.nextInt(sound.length);
		makeVoice(sound[select]);
	}
}