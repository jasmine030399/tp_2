import java.io.BufferedReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import javari.park.*;
import javari.reader.*;
import javari.park.*;
import javari.animal.*;
import javari.writer.*;
import java.io.IOException;
import java.util.*;

public class JavariPark{
    static CsvReader a;
    static CsvReader b;
    static CsvReader c;
    static Scanner inputan = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n... Opening default section database from data.");
        String directory = System.getProperty("user.dir") + "\\data";

        while(true){
            try{
                //buat objek csv reader berdasarkan file yg dibaca
                a = new AnimalCategories(Paths.get(directory, "animals_categories.csv"));
                b = new AnimalAttraction(Paths.get(directory, "animals_attractions.csv"));
                c = new AnimalRecord(Paths.get(directory, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            }
			catch (IOException ex){
                System.out.print("... File not found or incorrect file!\n\nPlease provide the source data path: ");
                directory = inputan.nextLine();
                directory = directory.replace("\\", "\\\\");
            }
        }

        //manggil method dari objek untuk hitung jumlah data yang valid & invalid
        System.out.println("Found " + a.countValidRecords() + " valid sections and " + a.countInvalidRecords() + " invalid sections");
        System.out.println("Found " + b.countValidRecords() + " valid attractions and " + b.countInvalidRecords() + " invalid attractions");
        System.out.println("Found " + a.countValidRecords() + " valid categories and " + a.countInvalidRecords() + " invalid categories");
        System.out.println("Found " + c.countValidRecords() + " valid records and " + c.countInvalidRecords() + " invalid records");
        
        mainMenu();

    }

    //membuat method print output pertama
    public static void mainMenu(){

        System.out.print("\nWelcome to Javari Park Festival - Registration Service!\n\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        nextMenu();
    }

    
    public static void nextMenu(){
        while(true){

            System.out.println("\nJavari Park has 3 sections:");
            System.out.println("1. Explore the Mammals");
            System.out.println("2. World of Aves");
            System.out.println("3. Reptilian Kingdom");
            System.out.print("Please choose your preferred section (type the number): ");
            String input = inputan.nextLine();
            switch(input){
                case "1":
                    sectionMenu("Explore the Mammals");
                    break;
                case "2":
                    sectionMenu("World of Aves");
                    break;
                case "3":
                    sectionMenu("Reptilian Kingdom");
                    break;
                case "#":
                    System.out.println("Farewell!");
                    System.exit(0);
                default:
                    System.out.println("Please enter a valid command!");
                    continue;
            }
        }

    }

    //method untuk milih binatang
    public static void sectionMenu(String section){
        while(true){
            System.out.println("\n--" + section + "--");
            switch(section){
                case "Explore the Mammals":
                int l = 1;
                for(int x = 0; x < Mammals.getMamal().length; x++){
                    System.out.println(l + ". " + Mammals.getMamal()[x]);
                    l++;
                }
                System.out.print("Please choose your preferred animals (type the number): ");
                String masukan1 = inputan.nextLine();
                    switch(masukan1){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Hamster");
                            break;
                        case "2":
                            attractionMenu("Lion");
                            break;
                        case "3":
                            attractionMenu("Cat");
                            break;
                        case "4":
                            attractionMenu("Whale");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
                case "World of Aves":
                    int k = 1;
                    for(int x = 0; x < Aves.getBurung().length; x++){
                        System.out.println(k + ". " + Aves.getBurung()[x]);
                        k++;
                    }
                    System.out.print("Please choose your preferred hewans (type the number): ");
                    String masukan2 = inputan.nextLine();
                    switch(masukan2){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Parrot");
                            break;
                        case "2":
                            attractionMenu("Eagle");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
                case "Reptilian Kingdom":
                    int i = 1;
                    for(int x = 0; x < Reptiles.getReptil().length; x++){
                        System.out.println(i + ". " + Reptiles.getReptil()[x]);
                        i++;
                    }
                    System.out.print("Please choose your preferred hewans (type the number): ");
                    String masukan3 = inputan.nextLine();
                    switch(masukan3){
                        case "#":
                            nextMenu();
                            break;
                        case "1":
                            attractionMenu("Snake");
                            break;
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                    break;
            }

        }
    }

    //method untuk milih atraksi
    public static String attractionMenu(String hewan){
        while(true){
            int  o  = 1;
            System.out.println("\n---" + hewan + "---");
            System.out.println("Attractions by " + hewan + ":");
            Attraction atraksi = new Attraction();
            int size = atraksi.findAnimal(hewan).size();
            if(size == 0){
                return "Unfortunately, no " + hewan + " can perform any attraction, please choose other hewans";
            }else{
                for(int z = 0; z < size; z++){
                    System.out.println(o + ". " + atraksi.findAnimal(hewan).get(z).getName());
                    o += 1;
                }
    
                System.out.print("Please choose your preferred attractions (type the number): ");
                String input = inputan.nextLine();
                if(input.equalsIgnoreCase("#")){
                    if(Arrays.asList(Mammals.getMamal()).contains(hewan)){
                        sectionMenu("Explore the Mammals");
                    }else if(Arrays.asList(Aves.getBurung()).contains(hewan)){
                        sectionMenu("World of Aves");
                    }else if(Arrays.asList(Reptiles.getReptil()).contains(hewan)){
                        sectionMenu("Reptillian Kingdom");
                    }
                    break;
                }else{
                    try{
                        int angka = Integer.parseInt(input);
                        if(angka < o){
                            lastCheck(atraksi.findAnimal(hewan).get(angka));
                            break;
                        }else{
                            System.out.println("Please enter a valid command!");
                            continue;
                        }
                    }catch(NumberFormatException a){
                        System.out.println("Please enter a valid command!");
                        continue;
                    }
                }
    
            }

        }
        return "";

    }

    //membuat method untuk memeriksa data
    public static void lastCheck(Attraction atraksi){
        while(true){
            
            System.out.print("\nWow, one more step,\nplease let us know your name: ");
            String namaPengunjung = inputan.nextLine();
            Registration np = Pengunjung.find(namaPengunjung);
            if(np == null){
                np = new Pengunjung(namaPengunjung);
            }
            System.out.println("\nYeay, final check!\nHere is your data, and the attraction you chose:");
            System.out.println("Name: " + namaPengunjung);
            System.out.println("Attractions: " + atraksi.getName() + " -> " + atraksi.getType());
            String hasil = "";
            for(int x = 0; x < atraksi.getPerformers().size(); x++){
                String nama =  atraksi.getPerformers().get(x).getName();
                if(nama.length() == 1){
                    nama = nama.toUpperCase();
                }else{
                    nama = nama.substring(0,1).toUpperCase() + nama.substring(1);
                }
                if(x != atraksi.getPerformers().size()-1){
                    hasil += nama + ", ";
                }else{
                    hasil += nama;
                }
            }
            System.out.println("With: " + hasil);
            System.out.print("\nIs the data correct? (Y/N): ");
            String choice = inputan.nextLine();
            switch(choice){
                case "Y":
                    np.addSelectedAttraction(atraksi);
                    System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
                    String secondchoice = inputan.nextLine();
                    switch(secondchoice){
                        case "Y":
                            nextMenu();
                            break;
                        case "N":
                                String hasil1 = np.getVisitorName().replace(" ", ",");
                                System.out.println("... End of program, write to registration_" + hasil1 + ".json ...");
                                System.exit(0);
                            break;  
                        default:
                            System.out.println("Please enter a valid command!");
                            continue;
                    }
                case "N":
                    System.out.println("Please re-fill the data!");
                    nextMenu();
                    break;
                default:
                    System.out.println("Please enter a valid command!");
                    continue;
                }
		}
	}
}

