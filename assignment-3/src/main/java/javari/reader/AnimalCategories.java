package javari.reader;
import javari.park.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.ArrayList;
import javari.animal.*;

public class AnimalCategories extends CsvReader{
	public AnimalCategories(Path file) throws IOException{
		super(file);
	}
	
	public ArrayList<String> kategori = new ArrayList<String>();
	
	public long countValidRecords(){
		Set<String> valid = new LinkedHashSet<>();
		for(int k = 0 ; k< lines.size(); k++){
			String baris = lines.get(k);
			String[] split_baris = baris.split(",");
			
			if(split_baris[1].equals("Mammals")){
				if(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2]) && Arrays.asList(Mammals.getMamal()).contains(split_baris[0])){
					valid.add(split_baris[2]);
				}
			}
			
			if(split_baris[1].equals("Aves")){
				if(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2]) && Arrays.asList(Aves.getBurung()).contains(split_baris[0])){
					valid.add(split_baris[2]);
				}
			}
			
			if(split_baris[1].equals("Reptiles")){
				if(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2]) && Arrays.asList(Reptiles.getReptil()).contains(split_baris[0])){
					valid.add(split_baris[2]);
				}
			}
		}
		return (long)valid.size();
	}
	
	public long countInvalidRecords(){
		long invalid = 0;
		for(int i = 0;i<lines.size();i++){
			String baris = lines.get(i);
			String[] split_baris = baris.split(",");
			
			if(split_baris[1].equals("Mammals")){
				if(!(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2])) && (!(Arrays.asList(Mammals.getMamal()).contains(split_baris[0])))){
					invalid = invalid + 1;
				}
			}
			
			if(split_baris[1].equals("Aves")){
				if(!(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2])) && (!(Arrays.asList(Aves.getBurung()).contains(split_baris[0])))){
					invalid = invalid + 1;
				}
			}
			
			if(split_baris[1].equals("Reptiles")){
				if(!(Arrays.asList(Attraction.getValidCategories()).contains(split_baris[2])) && (!(Arrays.asList(Reptiles.getReptil()).contains(split_baris[0])))){
					invalid = invalid + 1;
				}
			}
		}
		return invalid;
	}
}
			
			
				