package javari.reader;
import javari.park.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

public class AnimalAttraction extends CsvReader{
	public AnimalAttraction(Path file) throws IOException{
		super(file);
	}
	
	public long countValidRecords(){
		Set<String> valid = new LinkedHashSet<>();
		for(int i = 0; i< lines.size(); i++){
			String baris = lines.get(i);
			String[] split_baris = baris.split(",");
			
			if(Arrays.asList(Attraction.getValidAttraction()).contains(split_baris[1])){
		switch(split_baris[1]){
			case "Circles of Fires":
				if(CirclesOfFires.cekValid(split_baris[0])){
					new Attraction("Circles of Fires", split_baris[0]);
					valid.add(split_baris[1]);
				}
				break;
			case "Counting Masters":
				if(CountingMasters.cekValid(split_baris[0])){
					new Attraction("Counting Masters", split_baris[0]);
					valid.add(split_baris[1]);
				}
				break;
			case "Dancing Animals":
				if(DancingAnimals.cekValid(split_baris[0])){
					new Attraction("Dancing Animals", split_baris[0]);
					valid.add(split_baris[1]);
				}
				break;
			case "Passionate Coders":
				if(PassionateCoders.cekValid(split_baris[0])){
					new Attraction("Passionate Coders", split_baris[0]);
					valid.add(split_baris[1]);
				}
				break;
			}
                
			}
        }
        return (long)valid.size();

	}
	
	public long countInvalidRecords(){
		long invalid = 0;
		for(int i = 0; i< lines.size(); i++){
			String baris = lines.get(i);
			String[] split_baris = baris.split(",");
			if(Arrays.asList(Attraction.getValidAttraction()).contains(split_baris[1])){
				if(split_baris[1].equals("Circles of Fires")){
					if(CirclesOfFires.cekValid(split_baris[0])==false){
						invalid = invalid +1;
					}else{
						continue;
					}
				}
				if(split_baris[1].equals("Counting Masters")){
					if(CountingMasters.cekValid(split_baris[0])==false){
						invalid = invalid +1;
					}else{
						continue;
					}
				}
				if(split_baris[1].equals("Passionate Coders")){
					if(PassionateCoders.cekValid(split_baris[0])==false){
						invalid = invalid +1;
					}else{
						continue;
					}
				}
				if(split_baris[1].equals("Dancing Animals")){
					if(DancingAnimals.cekValid(split_baris[0])==false){
						invalid = invalid +1;
					}else{
						continue;
					}
				}
			}else{
				invalid = invalid +1;
			}
		}
		return invalid;
	}
}	