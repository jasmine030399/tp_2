package javari.reader;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class AnimalRecord extends CsvReader{

    long valid = 0;
    int counter = 0;

    public AnimalRecord(Path file) throws IOException{
        super(file);
    }
    
    //proses record-record dari file
    public void count(){
        for(int i = 0; i < lines.size(); i++){
            Animal z = null;
            String input = lines.get(i);
            String[] split_input = input.split(",");
            Integer a =  Integer.valueOf(split_input[0]);
            String b = split_input[1];
            String c = split_input[2];
            Gender d = Gender.parseGender(split_input[3]);
            double e = Double.parseDouble(split_input[4]);
            double f = Double.parseDouble(split_input[5]);
            String g = split_input[6];
            Condition h = Condition.parseCondition(split_input[7]);
            
			if(Arrays.asList(Mammals.getMamal()).contains(b)){
                z = new Mammals(a,b,c,d,e,f,g,h);
                valid += 1;
            }
			else if(Arrays.asList(Aves.getBurung()).contains(b)){
                z = new Aves(a,b,c,d,e,f,g,h);
                valid += 1;
            }
			else if(Arrays.asList(Reptiles.getReptil()).contains(b)){
                z = new Reptiles(a,b,c,d,e,f,g,h);
                valid += 1;
            }
			
            //tambah hewan ke dalam atraksi
            for(int y = 0; y < Attraction.getAttractionList().size(); y++){
                if(Attraction.getAttractionList().get(y).getType().equalsIgnoreCase(b)){
                    Attraction.getAttractionList().get(y).addPerformer(z);
                }
            }

        }
        counter += 1;
    }

    public long countValidRecords(){
        if(counter == 0){
            count();
        }
        return valid;
    }

    public long countInvalidRecords(){
        return lines.size() - countValidRecords();
    }
}
