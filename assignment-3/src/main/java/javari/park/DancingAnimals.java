package javari.park;
import java.util.*;

public class DancingAnimals extends Attraction{
	private static String[] animal_valid = {"Parrot", "Snake", "Cat", "Hamster"};
	private ArrayList<String> animal_lst = new ArrayList<String>();
	
	public DancingAnimals(String name, String type){
		super(name, type);
	}
	
	public static boolean cekValid(String type){
		for(int i = 0; i < animal_valid.length; i++){
			if(type.equals(animal_valid[i])){
				return true;
			}
			
		}
		return false;
	}
}