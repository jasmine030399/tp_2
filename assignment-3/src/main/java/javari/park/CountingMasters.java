package javari.park;
import java.util.*;

public class CountingMasters extends Attraction{
	private static String[] animal_valid = {"Hamster", "Whale", "Parrot"};
	private ArrayList<String> animal_lst = new ArrayList<String>();
	
	public CountingMasters(String name, String type){
		super(name, type);
	}
	
	public static boolean cekValid(String type){
		for(int i = 0; i < animal_valid.length; i++){
			if(type.equals(animal_valid[i])){
				return true;
			}
		}
		return false;
	}
}