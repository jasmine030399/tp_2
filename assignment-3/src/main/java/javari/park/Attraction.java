package javari.park;

import javari.park.*;
import javari.animal.*;
import java.util.*;


public class Attraction implements SelectedAttraction{
	private String name;
	private String type;
	private List<Animal> performer = new ArrayList<Animal>();
	private static ArrayList<Attraction> list_attraction = new ArrayList<Attraction>();
	private static String[] validCategories = {"Explore the Mammals", "Reptillian Kingdom", "world of Aves"};
	private static String[] validAttraction = {"Circle of Fires", "Counting Master", "Dancing Animals", "Passionate Coders"};
	private ArrayList<Attraction> atraksi = new ArrayList<Attraction>();
	
	public Attraction(){
		
	}
	
	
	public Attraction(String name, String type){
		this.name = name;
		this.type = type;
		list_attraction.add(this);
	}
	
	
	public static String[] getValidAttraction(){return validAttraction;}
	
	public static String[] getValidCategories(){return validCategories;}
	
	public String getName(){return name;}
	
	public String getType(){return type;}
	
	public List<Animal> getPerformers(){return performer;}
	
	public boolean addPerformer(Animal yangtampil){
		if(yangtampil.isShowable()){
			performer.add(yangtampil);
			return true;
		}
		return false;
	}
	
	public static ArrayList<Attraction> getAttractionList() {
        return list_attraction;
    }

	
	public  ArrayList<Attraction> findAnimal(String animal){
		for(int i = 0; i < list_attraction.size() ; i++){
			if(list_attraction.get(i).getType().equals(animal)){
				atraksi.add(list_attraction.get(i));
			}
		}
		return atraksi;
	}
}
	
	