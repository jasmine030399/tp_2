package javari.park;
import java.util.*;

public class PassionateCoders extends Attraction{
	private static String[] animal_valid = {"Hamster", "Cat", "Snake"};
	private ArrayList<String> animal_lst = new ArrayList<String>();
	
	public PassionateCoders(String name, String type){
		super(name, type);
	}
	
	public static boolean cekValid(String type){
		for(int i = 0; i < animal_valid.length; i++){
			if(type.equals(animal_valid[i])){
				return true;
			}
		}
		return false;
	}
}