package javari.park;
import java.util.*;

public class Pengunjung implements Registration{
	private String name;
	private int id;
	private int RegisCount = 0;
	
	private static ArrayList<Registration> pengunjung_lst = new ArrayList<Registration>();
	private static ArrayList<SelectedAttraction> attraction_lst = new ArrayList<SelectedAttraction>();
	
	public Pengunjung(String name){
		this.name = name;
		this.id = id;
		pengunjung_lst.add(this);
		RegisCount = RegisCount + 1;
	}
	
	public int getRegistrationId(){return id;}
	
	public String getVisitorName(){return this.name;}
	public void setVisitorName(String name){this.name = name;}
	
	public List<SelectedAttraction> getSelectedAttractions(){return attraction_lst;}

	public boolean addSelectedAttraction(SelectedAttraction selected){
		attraction_lst.add(selected);
		return true;
	}
	
	public static Registration find(String name){
		for(int j = 0; j < pengunjung_lst.size(); j++){
			if(pengunjung_lst.get(j).getVisitorName().equalsIgnoreCase(name)){
				return pengunjung_lst.get(j);
			}
		}
		return null;
	}
}
