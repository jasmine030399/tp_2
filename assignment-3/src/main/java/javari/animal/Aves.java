package javari.animal;

import java.util.*;
import javari.animal.*;

public class Aves extends Animal{
	private static String[] aves = {"Parrot" ,"Eagle"};
	private boolean isLayingEggs = false;
	
	public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String hewan, Condition condition){
		super(id, type, name, gender, length, weight, condition);
		
		if(hewan.equalsIgnoreCase("laying eggs")){
			this.isLayingEggs = true;
		}
	}
	
	public boolean specificCondition(){
		return (!(isLayingEggs));
	}
	
	public static String[] getBurung(){
		return aves;
	}
}