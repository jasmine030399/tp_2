package javari.animal;
import javari.animal.*;
import java.util.*;



public class Mammals extends Animal{
	private static String[] mammals = { "Hamster", "Lion", "Cat", "Whale"};
	private boolean isPregnant = false;
	
	public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, String hewan, Condition condition){
		super(id, type, name, gender, length, weight, condition);
		
		if(hewan.equalsIgnoreCase("pregnant")){
			this.isPregnant = true;
		}
	}
	
	public boolean specificCondition(){
		return (!(isPregnant)) && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
	}
	
	public static String[] getMamal(){
		return mammals;
	}
}