package javari.animal;
import java.util.*;
import javari.animal.*;

public class Reptiles extends Animal{
	private static String[] reptile = {"Snake"};
	private boolean isTame = false;

	public Reptiles(Integer id, String type, String name, Gender gender, double length, double weight, String hewan, Condition condition){
		super(id, type, name, gender, length, weight, condition);
		if(hewan.equalsIgnoreCase("tamed")){
			this.isTame = true;
		}
	}
	
	public boolean specificCondition(){
		return !(isTame);
	}
	
	public static String[] getReptil(){
		return reptile;
	}
}