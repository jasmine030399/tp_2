import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.JOptionPane;

public class Board extends JFrame{
	private List<Card> cards;
	private Card kartu1;
	private Card kartu2;
	private Timer waktu;
	private int tries = 0;
	private int reset = 0;
	private JFrame frame;
	private JLabel tries_label;
	private JLabel reset_label;
	private int reveal = 0;
	
	public Board(){
		int pasang = 18;
		List<Card> cardList = new ArrayList<>();
		List<Integer> cardval = new ArrayList<>();
		
		for(int i = 0 ; i < pasang; i++){
			cardval.add(i);
			cardval.add(i);
		}
		Collections.shuffle(cardval);
		
		for(int i : cardval){
			String[] hewan = {"img1", "img2", "img3", "img4", "img5", "img6",  
			"img7", "img8", "img9", "img10", "img11", "img12", "img13", "img14", "img15", 
			"img16", "img17", "img18"};
			
			
			Card kartu = new Card(i, resizeIcon(new ImageIcon("logo.png")), resizeIcon(new ImageIcon(hewan[i] + ".png")));
			kartu.getTombol().addActionListener(e -> doTurn(kartu));
			cardList.add(kartu);
			
			
		}
		this.cards = cardList;
		//untuk mengatur waktu
		waktu = new Timer(750, e -> checkCards());
		waktu.setRepeats(false);
		
		frame = new JFrame("Match-Pair Memory Game");
		frame.setLayout(new BorderLayout());
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(6,6));
		for(Card kartu : cards){
			panel.add(kartu.getTombol());
		}
		
		JPanel panel1 = new JPanel(new GridLayout(2,2));
		JButton reveal_button = new JButton("Reveal all(One Time)");
		reveal_button.addActionListener(e -> allReveal(cards));
		JButton reset_button = new JButton("Reset");
		panel1.add(reveal_button);
		panel1.add(reset_button);
		
		tries_label = new JLabel("Number of Tries : " + reset);
		panel1.add(tries_label);
		reset_label = new JLabel("Number of Reset : " + reset);
		panel1.add(reset_label);
		reset_button.addActionListener(e -> reset(cards));
		frame.add(panel, BorderLayout.CENTER);
		frame.add(panel1, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(600,600));
		frame.setLocation(300, 100);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	private void reset(List<Card> cards){
		reset += 1;
		reset_label.setText("Number of Reset: "+reset);
		for(Card kartu : cards){
			kartu.setMatch(false);
			kartu.release();
			kartu.getTombol().setEnabled(true);
		}
	}
	
	private void allReveal(List<Card> cards){
		if(reveal >=1){
			JOptionPane.showMessageDialog(this, "Limit reveal exceeded");
			return;
		}
		reveal += 1;
		Timer t1= new Timer(1500, e -> allHidden(cards));
		t1.setRepeats(false);
		for(Card kartu : cards){
			kartu.tekan();
		}
		t1.start();
	}
	
	private void allHidden(List<Card> cards){
		for(Card kartu : cards){
			if(!kartu.getMatch()){
				kartu.release();
			}
		}
	}
	
	private void doTurn(Card select_kartu){
		if(kartu1 == null && kartu2 == null){
			kartu1 = select_kartu;
			kartu1.tekan();
		}
		
		if(kartu1 != null && kartu1 != select_kartu && kartu2 == null){
			kartu2 = select_kartu;
			kartu2.tekan();
			waktu.start();
		}
	}
	
	private void checkCards(){
		if (kartu1.getId() == kartu2.getId()) { //match condition
            kartu1.getTombol().setEnabled(false); //disables the button
            kartu2.getTombol().setEnabled(false);
            kartu1.setMatch(true); 
            kartu2.setMatch(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!\nNumber of "
                        + "tries: " + tries + "\nNumber of Reset: " + reset);
                System.exit(0);
            }
        } else {
            kartu1.release();
            kartu2.release();
            tries += 1;
            tries_label.setText("Number of Tries: " + tries);
        }
        kartu1 = null; //reset kartu1 and kartu2
        kartu2 = null;
    }

    private boolean isGameWon() {
        for (Card kartu: this.cards) {
            if (!kartu.getMatch()) {
                return false;
            }
        }
        return true;
    }

    private static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(130, 120, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}
	
	
		
		
		
			
	
	