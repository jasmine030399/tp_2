import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Card{
	private final int id;
	private boolean match = false;
	private final JButton tombol = new JButton();
	private final ImageIcon tekan;
	private final ImageIcon icon;
	
	
	public Card(int id, ImageIcon icon, ImageIcon tekan){
		this.id = id;
		this.icon = icon;
		this.tekan = tekan;
		
		this.tombol.setIcon(icon);
	}
	
	public JButton getTombol(){return tombol;}
	
	
	public void tekan(){ tombol.setIcon(tekan);}
	
	public void release(){tombol.setIcon(icon);}
	
	public int getId(){return this.id;} //id 

	public void setMatch(boolean match){this.match = match;}
	public boolean getMatch(){return this.match;}
}
	
